Overview:
--------
Userreference_access is a very simple module to make it easier to edit 
(or simply see the tid) of terms.

By default it is available to everyone with administer taxonomy 
permissions.  If you wish to hide the block from people (the only 
feature so far) use block visibility by user role.

Please, if you have any other access control modules you would like to 
have work
with this module, please let me know:  http://agaricdesign.com/contact

Installation and configuration:
------------------------------
Please refer to the INSTALL file for complete installation and 
configuration instructions.


Requires:
--------
 - Drupal 5
 - enabled cck.module
 - enabled userreference.module


Credits:
-------
 - Written and maintained by Benjamin Melan�on of Agaric Design 
Collective
   http://agaricdesign.com/

 - Make donations to http://pwgd.org/
